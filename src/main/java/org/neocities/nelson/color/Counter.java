package org.neocities.nelson.color;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.type.TypeReference;
import org.neocities.nelson.color.model.Color;
import org.neocities.nelson.color.model.RGB;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/**
 * Created by dacruzgo on 2015-11-12.
 */
public class Counter {

    //public static final String BASE_PATH = "D:\\repository\\openshift\\color-converter\\src\\main\\resources\\";
    //public static final String INPUT_PATH = BASE_PATH.concat("source\\");
    //public static final String OUTPUT_PATH = BASE_PATH.concat("output\\");


    private static final Map<RGB, Integer> COUNTS = new HashMap<RGB, Integer>();
    public static final String TAB = "\t";

    private class ComparatorRGB implements Comparator<RGB> {

        public int compare(final RGB left, final RGB right) {
            final int l = left.getR() + left.getG() + left.getB();
            final int r = right.getR() + right.getG() + right.getB();
            return l - r;
        }
    }


    public static void main(final String[] args) throws IOException {
        new Counter().run(args[0]);
    }
    public void run(final String output) throws IOException {
        System.out.println("Starting");

        final File dir = new File(/*BASE_PATH.concat*/(output));
        final File[] files = dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.endsWith(".json");
            }
        });

        System.out.println(files.length + " palletes");

        final ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationConfig.Feature.INDENT_OUTPUT);


        for (final File file : files) {
            final List<Color> list = mapper.readValue(file, new TypeReference<List<Color>>(){});
            for (final Color color : list) {
                final RGB rgb = color.getRgb();
                if (COUNTS.get(rgb) != null) {
                    final Integer count = COUNTS.get(rgb);
                    COUNTS.put(rgb, count + 1);
                } else {
                    COUNTS.put(rgb, 1);
                }
            }
            //System.out.println(list);
        }

        final List<RGB> rgbs = new ArrayList<RGB>(COUNTS.keySet());
        Collections.sort(rgbs, new ComparatorRGB());

        final File file = new File(output.concat("rgb.lst"));
        final PrintWriter writer = new PrintWriter(file);
        for (final RGB rgb : rgbs) {
            writer.println(rgb.getR() + TAB + rgb.getG() + TAB + rgb.getB() + TAB + COUNTS.get(rgb));
        }
        writer.flush();
        writer.close();

        System.out.println(COUNTS.size() + " unique colors");
        int total = 0;
        for (final Integer value : COUNTS.values()) {
            total += value;
        }
        System.out.println(total + " total colors");

        COUNTS.clear();
    }

}
