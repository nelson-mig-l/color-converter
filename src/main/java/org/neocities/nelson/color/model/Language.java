package org.neocities.nelson.color.model;

/**
 * Created by dacruzgo on 2015-10-24.
 * https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes
 */
public enum Language {
    /** french */
    fr,
    /** german */
    de,
    /** english */
    en,
    /** dutch */
    nl,
    /** italian */
    it,
    /** spanish */
    es


}
