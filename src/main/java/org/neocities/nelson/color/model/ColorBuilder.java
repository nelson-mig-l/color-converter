package org.neocities.nelson.color.model;

import java.util.Map;

/**
 * Created by dacruzgo on 2015-10-22.
 */
public class ColorBuilder {

    private Color color = new Color();

    public static ColorBuilder builder() {
        return new ColorBuilder();
    }

    public ColorBuilder withName(final Language language, final String name) {
        final Map<Language, String> names = color.getNames();
        names.put(language, name);
        color.setNames(names);
        return this;
    }

    public ColorBuilder withRGB(final int r, final int g, final int b) {
        final RGB rgb = new RGB();
        rgb.setR(r);
        rgb.setG(g);
        rgb.setB(b);
        color.setRgb(rgb);
        return this;
    }

//    public ColorBuilder withPalette(final String palette) {
//        color.setPalette(palette);
//        return this;
//    }

    public Color build() {
        Color toReturn = color;

        if (toReturn.getCmyk() == null) {
            toReturn.setCmyk(ColorSpaceUtils.RGBtoCMYK(toReturn.getRgb()));
        }
        if (toReturn.getHsv() == null) {
            toReturn.setHsv(ColorSpaceUtils.RGBtoHSV(toReturn.getRgb()));
        }

        color = new Color();
        return toReturn;
    }


}
