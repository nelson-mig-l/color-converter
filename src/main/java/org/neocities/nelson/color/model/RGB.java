package org.neocities.nelson.color.model;

/**
 * Created by dacruzgo on 2015-10-24.
 */
public class RGB {

    private int r;
    private int g;
    private int b;

    public int getR() {
        return r;
    }

    public void setR(int r) {
        this.r = r;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public int getG() {
        return g;
    }

    public void setG(int g) {
        this.g = g;
    }

    @Override
    public boolean equals(final Object other) {
        if (other == this) return true;
        if (!(other instanceof RGB)) return false;
        final RGB that = (RGB)other;
        return (this.r == that.r) && (this.g == that.g) && (this.b == that.b);
    }

    @Override
    public int hashCode() {
        return (this.r << 16) + (this.g << 8) + this.b;
    }
}
