package org.neocities.nelson.color.model;

import java.math.BigDecimal;

/**
 * Created by dacruzgo on 2015-10-24.
 */
public class ColorSpaceUtils {

    private static final double BYTE = 255.0;

    public static CMYK RGBtoCMYK(final RGB rgb) {
        double r = Double.valueOf(rgb.getR());
        double g = Double.valueOf(rgb.getG());
        double b = Double.valueOf(rgb.getB());

        double white = max3(r / BYTE, g / BYTE, b / BYTE);
        double cyan = (white - r / BYTE) / white;
        double magenta = (white - g / BYTE) / white;
        double yellow = (white - b / BYTE) / white;
        double black = 1 - white;

        final CMYK cmyk = new CMYK();
        cmyk.setC(dp3(cyan));
        cmyk.setM(dp3(magenta));
        cmyk.setY(dp3(yellow));
        cmyk.setK(dp3(black));
        return cmyk;
    }

    public static HSV RGBtoHSV(final RGB rgb) {

        final HSV hsv = new HSV();

        double computedH = 0;
        double computedS = 0;
        double computedV = 0;

        double r = Double.valueOf(rgb.getR());
        double g = Double.valueOf(rgb.getG());
        double b = Double.valueOf(rgb.getB());

        r = r / BYTE;
        g = g / BYTE;
        b = b / BYTE;
        double minRGB = min3(r, g, b);
        double maxRGB = max3(r, g, b);

        // Black-gray-white
        if (minRGB == maxRGB) {
            computedV = minRGB;
            hsv.setH(0);
            hsv.setS(0);
            hsv.setV(computedV);
            return hsv;
        }

        // Colors other than black-gray-white:
        double d = (r == minRGB) ? g - b : ((b == minRGB) ? r - g : b - r);
        double h = (r == minRGB) ? 3 : ((b == minRGB) ? 1 : 5);
        computedH = 60 * (h - d / (maxRGB - minRGB));
        computedS = (maxRGB - minRGB) / maxRGB;
        computedV = maxRGB;

        hsv.setH(Math.round(computedH));
        hsv.setS(percent(computedS));
        hsv.setV(percent(computedV));
        return hsv;
    }

    private static double max3(double a, double b, double c) {
        return Math.max(Math.max(a, b), c);
    }

    private static double min3(double a, double b, double c) {
        return Math.min(Math.min(a, b), c);
    }

    private static double percent(double value) {
        return dp1(value * 100.0);
    }

    private static double dp3(double value) {
        if (Double.isNaN(value)) {
            return 0.0;
        }
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(3, BigDecimal.ROUND_HALF_UP);
        return bd.doubleValue();
    }

    private static double dp1(double value) {
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(1, BigDecimal.ROUND_HALF_UP);
        return bd.doubleValue();
    }

}
