package org.neocities.nelson.color.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dacruzgo on 2015-10-22.
 */
public class Color {

    private RGB rgb;
    private CMYK cmyk;
    private HSV hsv;

    private String _id;

    private Map<Language, String> names = new HashMap<Language, String>();
    private String palette;

    public String get_id() { return _id; }
    void set_id(final String id) {
        if (_id != null) throw new RuntimeException("_id was already set");
        this._id = id;
    }

    public RGB getRgb() { return rgb; }
    void setRgb(final RGB rgb) { this.rgb = rgb; }

    public HSV getHsv() { return hsv; }
    void setHsv(final HSV hsv) { this.hsv = hsv; }

    public CMYK getCmyk() { return cmyk; }
    void setCmyk(final CMYK cmyk) { this.cmyk = cmyk; }

    public Map<Language, String> getNames() { return names; }
    void setNames(final Map<Language, String> names) { this.names = names; }

    public String getPalette() { return palette; }
    void setPalette(final String palette) { this.palette = palette; }
}
