package org.neocities.nelson.color.model;

/**
 * Created by dacruzgo on 2015-10-24.
 */
public class HSV {
    private double h;
    private double s;
    private double v;

    public double getH() {
        return h;
    }

    void setH(double h) {
        this.h = h;
    }

    public double getS() {
        return s;
    }

    void setS(double s) {
        this.s = s;
    }

    public double getV() {
        return v;
    }

    void setV(double v) {
        this.v = v;
    }
}
