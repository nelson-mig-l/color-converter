package org.neocities.nelson.color.model;

/**
 * Created by dacruzgo on 2015-10-24.
 */
public class CMYK {

    private double c;
    private double m;
    private double y;
    private double k;

    public double getC() {
        return c;
    }

    void setC(double c) {
        this.c = c;
    }

    public double getM() {
        return m;
    }

    void setM(double m) {
        this.m = m;
    }

    public double getY() {
        return y;
    }

    void setY(double y) {
        this.y = y;
    }

    public double getK() {
        return k;
    }

    void setK(double k) {
        this.k = k;
    }
}
