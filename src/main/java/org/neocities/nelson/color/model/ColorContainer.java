package org.neocities.nelson.color.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by dacruzgo on 2015-10-30.
 */
public class ColorContainer {

    private final Set<Color> container = new HashSet<Color>();

    private class IdComparator implements Comparator<Color> {
        public int compare(final Color left, final Color right) {
            return left.get_id().compareTo(right.get_id());
        }
    }

    public boolean add(final Color color) {
        return container.add(color);
    }

    public int size() {
        return container.size();
    }

    public List<Color> colors(final String palette) {
        final Map<String, Color> result = new HashMap<String, Color>(container.size());
        for (final Color color : container) {
            color.setPalette(palette);
            color.set_id(generateId(color));
            if (result.containsKey(color.get_id())) {
                System.out.println("[XXX] " + color.getNames().values() + " " + result.get(color.get_id()).getNames().values());
            }
            result.put(color.get_id(), color);
        }

        final List<Color> values = new ArrayList<Color>(result.values());
        Collections.sort(values, new IdComparator());
        return values;
    }

    private String generateId(final Color color) {
        final String sRed = String.format("%02X", color.getRgb().getR());
        final String sGreen = String.format("%02X", color.getRgb().getG());
        final String sBlue = String.format("%02X", color.getRgb().getB());

        final String sName = String.format("%08X", color.getNames().get(Language.en).hashCode());
        final String sPalette = String.format("%08X", color.getPalette().hashCode());

        return sRed + sGreen + sBlue + sName + sPalette + "00";
    }

}
