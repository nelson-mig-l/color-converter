package org.neocities.nelson.color.convert;

import org.neocities.nelson.color.convert.html.GenericHtmlTableConverter;
import org.neocities.nelson.color.convert.text.TextFormat;
import org.neocities.nelson.color.convert.text.TextFormatParser;
import org.neocities.nelson.color.convert.xml.XmlFormat;

/**
 * Created by dacruzgo on 2015-10-27.
 */
public class ConverterFactory {

    public static Converter converter(final TextFormat format) {
        switch (format) {
            case AutoCADPalette:
                return new XmlConverter();
            case EPaintHtmlPalette:
                return new HtmlEPaintConverter();
            case ReseneHtmlFormat:
                return new HtmlReseneConverter();
            case GenericHtmlTableFormat:
                return new GenericHtmlTableConverter();
            default:
                final TextFormatParser formatParser = format.getParser();
                return new PlainTextConverter(formatParser);
        }
    }

    public static XmlConverter converter(final XmlFormat format) {
        return new XmlConverter();
    }
}
