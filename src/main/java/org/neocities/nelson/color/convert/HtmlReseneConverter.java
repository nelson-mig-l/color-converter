package org.neocities.nelson.color.convert;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.neocities.nelson.color.extract.ExtractUtils;
import org.neocities.nelson.color.model.Color;
import org.neocities.nelson.color.model.ColorBuilder;
import org.neocities.nelson.color.model.ColorContainer;
import org.neocities.nelson.color.model.Language;

import java.io.File;
import java.io.IOException;

/**
 * Created by Nelson on 10/27/2016.
 */
public class HtmlReseneConverter implements Converter {
    @Override
    public ColorContainer convert(File file) throws IOException {
        final ColorContainer container = new ColorContainer();
        final Document doc = Jsoup.parse(file, "UTF-8");

        final Elements matches = doc.getElementsByAttributeValueContaining("onclick", "click_item");
        //matches.get(0).getElementsByTag("td").get(3).text()
        for (final Element match : matches) {
            final String n = match.getElementsByTag("td").get(3).text();
            System.out.println(n);
            final Element cl = match.getElementsByTag("td").get(0);
            final Elements bgcolor = cl.getElementsByAttribute("bgcolor");
            if (bgcolor.isEmpty()) continue;
            final String hexRgb = bgcolor.get(0).attr("bgcolor");
            System.out.println(hexRgb);
            final Color color = ColorBuilder.builder()
                    .withName(Language.en, n)
                    .withRGB(ExtractUtils.getRed(hexRgb), ExtractUtils.getGreen(hexRgb), ExtractUtils.getBlue(hexRgb))
                    .build();
            container.add(color);
        }



        return container;
    }

    public static void main(final String[] args) throws IOException {
        new HtmlReseneConverter().convert(new File("D:/repository/openshift/color-data/source-unused/resene-current/Order Resene Paint Drawdowns - Woodsman.html"));
    }
}
