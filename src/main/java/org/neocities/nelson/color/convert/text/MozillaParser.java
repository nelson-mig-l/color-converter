package org.neocities.nelson.color.convert.text;

import org.neocities.nelson.color.model.Color;
import org.neocities.nelson.color.model.ColorBuilder;
import org.neocities.nelson.color.model.Language;

/**
 * Created by dacruzgo on 2015-10-29.
 */
public class MozillaParser implements TextFormatParser {

    private final ColorBuilder builder = ColorBuilder.builder();

    public boolean canParse(final String line) {
        final String[] tokens = line.split(":");
        return tokens.length == 2 && line.contains("NS_RGB");
    }

    public Color parse(final String line) {
        final String[] tokens = line.split(":");
        final String name = tokens[0];
        final String[] rgb = tokens[1].split(",");
        final int r = Integer.valueOf(rgb[0].substring(8).trim());
        final int g = Integer.valueOf(rgb[1].trim());
        final int b = Integer.valueOf(rgb[2].replace(")", "").trim());
        builder.withName(Language.en, name);
        builder.withRGB(r, g, b);
        return builder.build();
    }
}
