package org.neocities.nelson.color.convert.text;

/**
 * Created by dacruzgo on 2015-10-26.
 */
public enum TextFormat {
    XKCD(new XkcdParser()),
    PerlColorLibrary(new PerlColorLibraryParser()),
    X11(new X11Parser()),
    GenericCsv(new GenericCsvParser()),
    Mozilla(new MozillaParser()),
    GimpPalette(new GimpPaletteParser()),
    // TODO HACK
    AutoCADPalette(null), EPaintHtmlPalette(null), ReseneHtmlFormat(null), GenericHtmlTableFormat(null);

    private final TextFormatParser parser;

    TextFormat(final TextFormatParser parser) {
        this.parser = parser;
    }

    public TextFormatParser getParser() {
        return parser;
    }

}