package org.neocities.nelson.color.convert.text;

import org.neocities.nelson.color.model.Color;
import org.neocities.nelson.color.model.ColorBuilder;
import org.neocities.nelson.color.model.Language;

import java.util.Arrays;
import java.util.List;

/**
 * Created by dacruzgo on 2015-10-29.
 */
public class GenericCsvParser implements TextFormatParser {

    // List of known headers
    private static final List<String> HEADER_1 = Arrays.asList("r", "g", "b", "color");
    private static final List<String> HEADER_2 = Arrays.asList("red", "green", "blue", "color name");
    private static final List<String> HEADER_3 = Arrays.asList("r", "g", "b", "name");

    private int rIndex;
    private int gIndex;
    private int bIndex;
    private int nIndex;

    private final ColorBuilder builder = ColorBuilder.builder();

    public boolean canParse(final String line) {
        final String[] tokens = line.toLowerCase().split(",");
        if (tokens.length < 4) {
            return false;
        }
        final List<String> columns = Arrays.asList(tokens);
        if (isHeader(columns)) {
            updateIndexes(columns);
            return false;
        }
        return true;
    }

    public Color parse(final String line) {
        final String[] tokens = line.split(",");
        builder.withName(Language.en, tokens[nIndex]);
        final Integer r = Integer.valueOf(tokens[rIndex]);
        final Integer g = Integer.valueOf(tokens[gIndex]);
        final Integer b = Integer.valueOf(tokens[bIndex]);
        builder.withRGB(r, g, b);
        return builder.build();
    }

    private boolean isHeader(final List<String> columns) {
        return columns.containsAll(HEADER_1) || columns.containsAll(HEADER_2) || columns.containsAll(HEADER_3);
    }

    private void updateIndexes(final List<String> columns) {
        rIndex = Math.max(columns.indexOf("r"), columns.indexOf("red"));
        gIndex = Math.max(columns.indexOf("g"), columns.indexOf("green"));
        bIndex = Math.max(columns.indexOf("b"), columns.indexOf("blue"));
        nIndex = Math.max(Math.max(columns.indexOf("color"), columns.indexOf("color name")), columns.indexOf("name"));
    }
}
