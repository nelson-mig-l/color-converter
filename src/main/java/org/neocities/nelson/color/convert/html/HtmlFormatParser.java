package org.neocities.nelson.color.convert.html;

import org.jsoup.nodes.Element;
import org.neocities.nelson.color.model.Color;

/**
 * Created by Nelson on 11/12/2016.
 */
public interface HtmlFormatParser {

    boolean canParse(Element element);
    Color parse(Element element);
}
