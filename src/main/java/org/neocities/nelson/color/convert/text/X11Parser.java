package org.neocities.nelson.color.convert.text;

import org.neocities.nelson.color.model.Color;
import org.neocities.nelson.color.model.ColorBuilder;
import org.neocities.nelson.color.model.Language;

/**
 * X11 format.
 * [Spacealigned]r[Spacealigned]g[Spacealigned]b[Spacealigned]color name
 */
public class X11Parser implements TextFormatParser {

    private final ColorBuilder builder = ColorBuilder.builder();

    public boolean canParse(final String line) {
        return !line.startsWith("!") && line.length() > 0;
    }

    public Color parse(final String line) {
        int r = Integer.valueOf(line.substring(0, 3).trim());
        int g = Integer.valueOf(line.substring(4, 7).trim());
        int b = Integer.valueOf(line.substring(8, 11).trim());

        String name = line.substring(13);

        builder.withName(Language.en, name).withRGB(r, g, b);
        return builder.build();
    }
}
