package org.neocities.nelson.color.convert;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.neocities.nelson.color.convert.text.TextFormatParser;
import org.neocities.nelson.color.model.ColorContainer;

import java.io.File;
import java.io.IOException;

/**
 * Created by dacruzgo on 2015-10-26.
 */
public class PlainTextConverter implements Converter {

    private final TextFormatParser format;

    PlainTextConverter(final TextFormatParser format) {
        this.format = format;
    }

    public ColorContainer convert(final File file) throws IOException {
        final ColorContainer colors = new ColorContainer();

        final LineIterator iterator = FileUtils.lineIterator(file);
        while (iterator.hasNext()) {
            final String line = iterator.nextLine();
            if (format.canParse(line)) {
                //System.out.println(line);
                colors.add(format.parse(line));
            }
        }
        return colors;
    }

}
