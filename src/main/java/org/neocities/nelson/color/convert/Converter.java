package org.neocities.nelson.color.convert;

import org.neocities.nelson.color.model.ColorContainer;

import java.io.File;
import java.io.IOException;

/**
 * Created by dacruzgo on 2015-10-29.
 */
public interface Converter {
    ColorContainer convert(final File file) throws IOException;
}
