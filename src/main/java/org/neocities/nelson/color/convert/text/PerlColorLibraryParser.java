package org.neocities.nelson.color.convert.text;

import org.neocities.nelson.color.model.Color;
import org.neocities.nelson.color.model.ColorBuilder;
import org.neocities.nelson.color.model.Language;

/**
 * Found NBS/ISCC colors in a perl library.
 * This will parse the perl source files.
 */
public class PerlColorLibraryParser implements TextFormatParser {
    private final ColorBuilder builder = ColorBuilder.builder();

    public boolean canParse(final String line) {
        return !line.isEmpty() && line.startsWith("[") && line.split(",").length >= 6;
    }

    /**
     * Parse line.
     * ['nbs-iscc-m:wildorchid.245','wildorchid','wild orchid',[131,100,121],'836479',8610937],
     * @param line
     * @return
     */
    public Color parse (final String line) {
        final String s = handleQuotations(line);
        //System.out.println(s);
        String[] tokens = s.split("%");
        String name = tokens[5].split(",")[0];
        String[] rgb = tokens[6].split(",");

        int r = Integer.valueOf(rgb[1].replace("[", ""));
        int g = Integer.valueOf(rgb[2]);
        int b = Integer.valueOf(rgb[3].replace("]", ""));

        builder.withRGB(r, g, b);
        return builder.withName(Language.en, name).build();
    }

    private String handleQuotations(final String line) {
        String result = line.replace("\\'", "<!>");
        result = result.replace("'", "%");
        return result.replace("<!>", "\\'");
    }

}
