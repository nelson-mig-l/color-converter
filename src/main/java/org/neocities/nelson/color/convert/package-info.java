/**
 * Convert.
 * Classes for simple format conversion.
 * Some data has complex format and needs to be processed by classes in extract package.
 */
package org.neocities.nelson.color.convert;