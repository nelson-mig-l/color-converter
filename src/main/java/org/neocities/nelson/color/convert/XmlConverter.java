package org.neocities.nelson.color.convert;

import org.neocities.nelson.color.model.Color;
import org.neocities.nelson.color.model.ColorBuilder;
import org.neocities.nelson.color.model.ColorContainer;
import org.neocities.nelson.color.model.Language;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.IOException;

/**
 * Created by Nelson on 10/26/2016.
 */
public class XmlConverter implements Converter {
    @Override
    public ColorContainer convert(File file) throws IOException {
        final ColorContainer colors = new ColorContainer();

        try {
            final DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            final DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            final Document doc = dBuilder.parse(file);
            doc.getDocumentElement().normalize();
            final NodeList nList = doc.getElementsByTagName("colorEntry");
            for (int index = 0; index < nList.getLength(); index++) {
                final Node item = nList.item(index);
                if (item.getNodeType() == Node.ELEMENT_NODE) {
                    final Element eElement = (Element) item;
                    final String colorName = eElement.getElementsByTagName("colorName").item(0).getTextContent();
                    final Element rgb = (Element) eElement.getElementsByTagName("RGB8").item(0);
                    final String red = rgb.getElementsByTagName("red").item(0).getTextContent();
                    final String green = rgb.getElementsByTagName("green").item(0).getTextContent();
                    final String blue = rgb.getElementsByTagName("blue").item(0).getTextContent();
                    //System.out.println(colorName + " "+ red + " " + green +" "+blue);
                    final Color color = ColorBuilder.builder()
                            .withName(Language.en, colorName)
                            .withRGB(Integer.valueOf(red), Integer.valueOf(green), Integer.valueOf(blue))
                            .build();
                    colors.add(color);
                }
            }
        } catch (Exception e) {
            throw new IOException(e);
        }
        return colors;
    }

    public static void main(final String[] args) throws IOException {
        new XmlConverter().convert(new File("D:\\repository\\openshift\\color-data\\source\\benjamin-moore\\BenjaminMoore_Affinity_en-us.acb"));
}
}
