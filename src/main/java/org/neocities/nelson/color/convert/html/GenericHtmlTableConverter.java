package org.neocities.nelson.color.convert.html;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.neocities.nelson.color.convert.Converter;
import org.neocities.nelson.color.model.Color;
import org.neocities.nelson.color.model.ColorContainer;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by Nelson on 11/12/2016.
 */
public class GenericHtmlTableConverter implements Converter {

    private final HtmlFormatParser parser = new GenericHtmlFormatParser();

    @Override
    public ColorContainer convert(final File file) throws IOException {
        final ColorContainer container = new ColorContainer();
        final Document doc = Jsoup.parse(file, "UTF-8");

        final Elements tbodies = doc.getElementsByTag("tbody");
        for (final Element body : tbodies) {
            final Elements rows = body.getElementsByTag("tr");
            for (final Element row : rows) {
                if (parser.canParse(row)) {
                    final Color color = parser.parse(row);
                    container.add(color);
                }
            }
        }

        return container;
    }

    public static void main(final String[] args) throws IOException {
        final File file = new File("D:\\repository\\openshift\\color-data\\source\\colorhexa\\List of colors.html");
        final ColorContainer colorContainer = new GenericHtmlTableConverter().convert(file);
        final List<Color> colors = colorContainer.colors("Color Hexa");
        for (final Color color : colors) {
            System.out.println(color);
        }
    }
}
