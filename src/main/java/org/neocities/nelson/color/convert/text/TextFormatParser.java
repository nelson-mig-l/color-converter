package org.neocities.nelson.color.convert.text;

import org.neocities.nelson.color.model.Color;

/**
 * Created by dacruzgo on 2015-10-26.
 */
public interface TextFormatParser {
    boolean canParse(String line);
    Color parse(String line);
}
