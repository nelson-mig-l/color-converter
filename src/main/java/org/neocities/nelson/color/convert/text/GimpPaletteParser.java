package org.neocities.nelson.color.convert.text;

import org.neocities.nelson.color.model.Color;
import org.neocities.nelson.color.model.ColorBuilder;
import org.neocities.nelson.color.model.Language;

/**
 * Created by dacruzgo on 2015-10-30.
 */
public class GimpPaletteParser implements TextFormatParser {

    private final ColorBuilder builder = ColorBuilder.builder();

    public boolean canParse(final String line) {
        return !line.contains(":") && !line.startsWith("GIMP") && !line.startsWith("#");
    }

    public Color parse(final String line) {
        int r = Integer.valueOf(line.substring(0, 3).trim());
        int g = Integer.valueOf(line.substring(4, 7).trim());
        int b = Integer.valueOf(line.substring(8, 11).trim());
        String name = line.substring(12);
        return builder.withRGB(r, g, b).withName(Language.en, name).build();
    }
}
