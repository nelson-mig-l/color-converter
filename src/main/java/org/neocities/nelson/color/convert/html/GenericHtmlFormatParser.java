package org.neocities.nelson.color.convert.html;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.neocities.nelson.color.model.Color;
import org.neocities.nelson.color.model.ColorBuilder;
import org.neocities.nelson.color.model.Language;

/**
 * Created by Nelson on 11/12/2016.
 */
public class GenericHtmlFormatParser implements HtmlFormatParser {
    @Override
    public boolean canParse(final Element element) {
        final Elements cells = element.getElementsByTag("td");
        return cells.size() == 8;
    }

    @Override
    public Color parse(final Element element) {
        final Elements cells = element.getElementsByTag("td");
        final String name = cells.get(0).text();
        final String red = cells.get(2).text();
        final String green = cells.get(3).text();
        final String blue = cells.get(4).text();
        final Color color = ColorBuilder.builder()
                .withName(Language.en, name)
                .withRGB(Integer.valueOf(red), Integer.valueOf(green), Integer.valueOf(blue))
                .build();
        return color;
    }
}
