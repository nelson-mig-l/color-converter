package org.neocities.nelson.color.convert;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.neocities.nelson.color.model.Color;
import org.neocities.nelson.color.model.ColorBuilder;
import org.neocities.nelson.color.model.ColorContainer;
import org.neocities.nelson.color.model.Language;

import java.io.File;
import java.io.IOException;

/**
 * Created by Nelson on 10/27/2016.
 */
public class HtmlEPaintConverter implements Converter {
    @Override
    public ColorContainer convert(final File file) throws IOException {
        final ColorContainer container = new ColorContainer();
        final Document doc = Jsoup.parse(file, "UTF-8");

        final Elements colorCharts = doc.getElementsByClass("colour-charts");
        if (colorCharts.size() != 1) throw new RuntimeException("colour-charts != 1");
        final Element theChart = colorCharts.get(0);
        final Elements cells = theChart.getElementsByTag("li");
        for (final Element cell : cells) {
            final String style = cell.getElementsByTag("a").get(0).getElementsByTag("img").get(0).attr("style");
            final String[] styleTokens = style.split(";");
            String rgb = styleTokens[2];
            rgb = rgb.replace(" background-color:rgb(", "");
            rgb = rgb.replace(" background-color: rgb(", "");
            rgb = rgb.replace(")", "");

            final String[] rgbTokens = rgb.split(",");
            final int r = Integer.valueOf(rgbTokens[0]);
            final int g = Integer.valueOf(rgbTokens[1]);
            final int b = Integer.valueOf(rgbTokens[2]);

            final String name = cell.getElementsByTag("h3").text();

            final Color color = ColorBuilder.builder()
                    .withName(Language.en, name)
                    .withRGB(r, g, b)
                    .build();

            container.add(color);

        }
        return container;
    }

    public static void main(final String[] args) throws IOException {
        new HtmlEPaintConverter().convert(new File("D:/repository/openshift/color-data/source/e-paint/British Standard BS381C COLOUR CHART for paints.html"));
    }
}
