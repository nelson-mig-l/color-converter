package org.neocities.nelson.color.convert.text;

import org.neocities.nelson.color.model.Color;
import org.neocities.nelson.color.model.ColorBuilder;
import org.neocities.nelson.color.model.Language;

/**
 * Xkcd format.
 * color name[TAB]#RRGGBB
 */
public class XkcdParser implements TextFormatParser {

    private final ColorBuilder builder = ColorBuilder.builder();

    public boolean canParse(final String line) {
        return !line.startsWith("#");
    }

    public Color parse(String line) {
        String[] tokens = line.split("\t");
        String name = tokens[0];
        builder.withName(Language.en, name);
        String hex = tokens[1];
        int r = Integer.valueOf(hex.substring(1, 3), 16);
        int g = Integer.valueOf(hex.substring(3, 5), 16);
        int b = Integer.valueOf(hex.substring(5, 7), 16);
        builder.withRGB(r, g, b);
        return builder.build();
    }

}
