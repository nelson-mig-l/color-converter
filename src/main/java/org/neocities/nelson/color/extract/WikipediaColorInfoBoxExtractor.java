package org.neocities.nelson.color.extract;

import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URL;

import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Created by dacruzgo on 2015-10-30.
 */
public class WikipediaColorInfoBoxExtractor implements Extractor {

    public void extract(final URL url,  final String outputDirectoryName) throws IOException {
        Document doc;
        System.out.println("=============================================================");
        System.out.println(url);
        System.out.println("=============================================================");
        try {
            doc = Jsoup.parse(url, 6000);
        } catch (HttpStatusException e) {
            System.err.println(e);
            return;
        }
        final String title = doc.title().split("-")[0].trim().replace(" ", "-");
        final File out = new File(outputDirectoryName.concat(ExtractUtils.EXTRACTED).concat(title).concat(".csv"));
        final FileWriter fw = new FileWriter(out);
        final Elements tables = doc.getElementsByTag("table");
        for (final Element table : tables) {
            if (table.classNames().contains("infobox") && table.html().contains("RGB")) {
                final Element trName = table.getElementsByTag("tr").get(0);
                final String name = trName.getElementsByTag("th").text();
                final String string = name + "," + find(table, "Hex triplet").text() + "," + find(table, "Source").text();
                fw.write(string + "\r\n");
                System.out.println(string);
            }
        }

        fw.flush();
        fw.close();
    }

    public int extract(final String inputFileName, final String outputDirectoryName) throws IOException {
        final Document doc = Jsoup.parse(new File(inputFileName), "UTF-8");
        final String title = doc.title().split("-")[0].trim().replace(" ", "-");
        final File out = new File(outputDirectoryName.concat(ExtractUtils.EXTRACTED).concat(title).concat(".csv"));
        final FileWriter fw = new FileWriter(out);
        final Elements tables = doc.getElementsByTag("table");
        for (final Element table : tables) {
            if (table.classNames().contains("infobox")) {
                final Element trName = table.getElementsByTag("tr").get(0);
                final String name = trName.getElementsByTag("th").text();
                fw.write(name + "," + find(table, "Hex triplet").text() + "," + find(table, "Source").text() + "\r\n");
                System.out.println(
                        name + "," + find(table, "Hex triplet").text() + "," + find(table, "Source").text()
                );
            }
        }

        fw.flush();
        fw.close();
        return 0;
    }

    private Element find(final Element table, final String text) {
        final Elements rows = table.getElementsByTag("tr");
        for (final Element row : rows) {
            final Elements cells = row.getElementsByTag("th");
            for (final Element cell : cells) {
                if (cell.text().contains(text)) {
                    return row.getElementsByTag("td").get(0);
                }
            }
        }
        return null;
    }

    public static void main(String[] args) throws IOException {
        String s = "C:\\workspaces\\xorlab\\openshift\\color-converter\\src\\main\\resources\\source\\ShadesOf";
        final String[] files = new File(s).list(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.endsWith(".html");
            }
        });
        for (final String file : files) {
            String f = s + "\\" + file;
            new WikipediaColorInfoBoxExtractor().extract(f, s);
        }
    }

}
