package org.neocities.nelson.color.extract;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by dacruzgo on 2015-11-14.
 */
public class EPaintTableExtractor implements Extractor {

    private static final String SEPARATOR = ",";
    private static final String EXTENSION = ".csv";

    public int extract(final String inputFileName, final String outputDirectoryName) throws IOException {
        final File input = new File(inputFileName);
        final File output = new File(getOutputFileNameFromInputFileName(inputFileName, outputDirectoryName));

        final Document doc = Jsoup.parse(input, "UTF-8");
        final Elements tables = doc.getElementsByClass("tablecolourchart");
        final Element table = tables.get(0);

        final FileWriter fw = new FileWriter(output);
        fw.write("r,g,b,name\r\n");

        final Elements subtables = table.getElementsByClass("subtablecolourchart");
        for (final Element subtable : subtables) {
            final Elements cells = subtable.getElementsByTag("td");
            for (final Element cell : cells) {
                final String[] tokens = cell.getElementsByTag("img").get(0).attr("style").split(";");
                String color = tokens[4];
                color = color.replace(" background-color:rgb(", "");
                color = color.replace(")", "");
                String name = cell.getElementsByTag("h3").get(0).text();
                if (name.contains("/")) {
                    final String[] ns = name.split("/");
                    for (final String n : ns) {
                        final String nn = n.trim();
                        System.out.println(color + " -- " + nn);
                        fw.write(color + "," + nn + "\r\n");
                    }
                } else {
                    System.out.println(color + " -- " + name);
                    fw.write(color + "," + name + "\r\n");
                }
            }
        }

        fw.flush();
        fw.close();

        System.out.println("=====================================================================");
        return 0;
    }

    private String getOutputFileNameFromInputFileName(final String inputFileName, final String outputDirectoryName) {
        final String name = new File(inputFileName).getName().replace(".html", ".csv");
        return outputDirectoryName.concat(ExtractUtils.EXTRACTED).concat(name);
    }

    public static void main(String[] args) throws IOException {
        //String s = "C:\\workspaces\\xorlab\\openshift\\color-converter\\src\\main\\resources\\source\\e-paint";
        String s = "D:\\repository\\openshift\\color-data\\source-unused\\e-paint";
        String f;

        f = s + "\\RAL Colour Chart - color charts, fans, cards, swatches and books..html";
        new EPaintTableExtractor().extract(f, s);


//        f = s + "\\Australian Standard colour chart.html";
//        new EPaintTableExtractor().extract(f, s);
//
//        f = s + "\\British Standard BS381C COLOUR CHART for paints.html";
//        new EPaintTableExtractor().extract(f, s);
//
//        f = s + "\\British Standard BS2660 COLOUR CHART.html";
//        new EPaintTableExtractor().extract(f, s);
//
//        f = s + "\\British Standard BS5252 COLOUR CHART.html";
//        new EPaintTableExtractor().extract(f, s);
//
//        f = s + "\\BS4800 COLOUR CHART_ 1989.html";
//        new EPaintTableExtractor().extract(f, s);
//
//        f = s + "\\British Standard BS 4800 COLOUR CHART for paints.html";
//        new EPaintTableExtractor().extract(f, s);
//
//        f = s + "\\Other British Standard Colour Chart.html";
//        new EPaintTableExtractor().extract(f, s);
//
//        f = s + "\\Federal standard 595 color chart.html";
//        new EPaintTableExtractor().extract(f, s);
    }

}
