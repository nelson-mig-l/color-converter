package org.neocities.nelson.color.extract;

import java.io.IOException;

/**
 * Created by dacruzgo on 2015-11-17.
 */
public interface Extractor {

    int extract(final String inputFileName, final String outputDirectoryName) throws IOException;

}
