package org.neocities.nelson.color.extract;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Created by dacruzgo on 2015-10-29.
 */
public class HtmlTableExtractor implements Extractor {

    private static final String SEPARATOR = ",";
    private static final String EXTENSION = ".csv";

    private String outputDirectoryName;

    public int extract(final String inputFileName, final String outputDirectoryName) throws IOException {
        this.outputDirectoryName = outputDirectoryName;
        extract(new File(inputFileName));
        return 0;
    }

    public void extract(File file) throws IOException {
        final Document doc = Jsoup.parse(file, "UTF-8");
        final Elements tables = doc.getElementsByTag("table");
        for (final Element table : tables) {
            process(table);
        }
    }

    private String getHeader(final Element table) {
        Element current = table;
        while (!current.tag().toString().startsWith("h")) {
            current = current.previousElementSibling();
            if (current == null) {
                return "[Not Found] " + Integer.toHexString(table.hashCode());
            }
        }
        return current.text();
    }

    private File process(final Element table) throws IOException {
        final String name = getHeader(table).replace(" ", "-").replace("'", "").toLowerCase();
        System.out.println(name);
        final String outname = outputDirectoryName.concat(ExtractUtils.EXTRACTED).concat(name).concat(".csv");
        final File file = new File(outname);
        final FileWriter fw = new FileWriter(file);
        final Elements rows = table.getElementsByTag("tr");
        for (final Element row : rows) {
            final Elements columns = row.getElementsByTag("th");

            String header = "";
            for (final Element column : columns) {
                header = header.concat(column.text().replace("\u00a0", "") + SEPARATOR);
            }
            if (!columns.isEmpty()) {
                header = header.substring(0, header.length() - 1);
                fw.write(header.concat("\r\n"));
            }

            String data = "";
            final Elements cells = row.getElementsByTag("td");
            for (final Element cell : cells) {
                data = data.concat(cell.text().replace("\u00a0","") + SEPARATOR);
            }
            if (!cells.isEmpty()) {
                data = data.substring(0, data.length() - 1);
                fw.write(data.concat("\r\n"));
            }
        }

        fw.flush();
        fw.close();
        return file;
    }

    public static void main(String[] args) throws IOException {
        String s;
        String f;

        s = "C:\\workspaces\\xorlab\\openshift\\color-converter\\src\\main\\resources\\source\\crayola";
        f = s + "\\List of Crayola crayon colors - Wikipedia, the free encyclopedia.html";
        //new HtmlTableExtractor(s.concat("\\_extracted\\crayola")).extract(new File(f));
        new HtmlTableExtractor().extract(f, s);

        s = "C:\\workspaces\\xorlab\\openshift\\color-converter\\src\\main\\resources\\source\\colorhexa";
        f = s + "\\colorhexa - List of colors.html";
        //new HtmlTableExtractor(s.concat("\\_extracted\\colorhexa")).extract(new File(f));
        new HtmlTableExtractor().extract(f, s);

    }

}
