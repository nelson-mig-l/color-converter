package org.neocities.nelson.color.extract;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

/**
 * Created by dacruzgo on 2015-11-17.
 */
public class BenjaminMooreExtractor implements Extractor {

    private static final String OBJ = "[{\"Locale\":\"\",\"RGB\":{\"B\":236,\"G\":242,\"R\":239},\"collectionCode\":\"AFF\",\"colorCode\":\"AF-5\",\"colorId\":108529,\"colorName\":\"Frostine\",\"colorShortURL\":\"\\/frostine\",\"gridLocation\":{\"Column\":0,\"Row\":0}}]";

    public static class RGBObj {
        public Integer B;
        public Integer G;
        public Integer R;
    }

    public static class LocObj {
        public Integer Column;
        public Integer Row;
    }

    public static class Obj {
        public String Locale;
        public RGBObj RGB;
        public String collectionCode;
        public String colorCode;
        public Integer colorId;
        public String colorName;
        public String colorShortURL;
        public LocObj gridLocation;
    }

    public int extract(final String inputFileName, final String outputDirectoryName) throws IOException {
        System.out.println("RUN for " + inputFileName);

        final String json = readFile(inputFileName);

        ObjectMapper mapper = new ObjectMapper();
        List<Obj> os = mapper.readValue(json, new TypeReference<List<Obj>>() { });

        final File output = new File(getOutputFileNameFromInputFileName(inputFileName, outputDirectoryName));
        final PrintWriter writer = new PrintWriter(output);
        writer.println("color,r,g,b");
        for (final Obj o : os) {
            writer.println(o.colorName + "," + o.RGB.R + "," + o.RGB.G + "," + o.RGB.B);
        }
        writer.flush();
        writer.close();
        return os.size();
    }

    private String getOutputFileNameFromInputFileName(final String inputFileName, final String outputDirectoryName) {
        final String name = new File(inputFileName).getName().replace(".json", ".csv");
        return outputDirectoryName.concat(ExtractUtils.EXTRACTED).concat(name);
    }

    private String readFile(final String path) throws IOException {
        final byte[] bytes = Files.readAllBytes(Paths.get(path));
        final String contents = new String(bytes);
        final String json = contents.replace("_jqjsp( ", "").replace(" );", "");
        return json;
    }

    public static void main(String[] args) throws IOException {
        String s = "C:\\workspaces\\xorlab\\openshift\\color-converter\\src\\main\\resources\\source\\benjaminmoore\\";
        final String[] files = new File(s).list(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.endsWith(".json");
            }
        });
        final Extractor extractor = new BenjaminMooreExtractor();
        for (final String file : files) {
            extractor.extract(s + file, s);
        }
    }

}
