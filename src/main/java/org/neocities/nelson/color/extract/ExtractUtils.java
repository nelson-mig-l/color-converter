package org.neocities.nelson.color.extract;

/**
 * Created by dacruzgo on 2015-10-30.
 */
public class ExtractUtils {

    public static String EXTRACTED = "\\_extracted\\";

    public static int getRed(final String hex) {
        return Integer.valueOf(hex.substring(1, 3), 16);
    }

    public static int getGreen(final String hex) {
        return Integer.valueOf(hex.substring(3, 5), 16);
    }

    public static int getBlue(final String hex) {
        return Integer.valueOf(hex.substring(5, 7), 16);
    }
}
