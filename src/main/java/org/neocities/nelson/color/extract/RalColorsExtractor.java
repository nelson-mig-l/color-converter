package org.neocities.nelson.color.extract;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Created by dacruzgo on 2015-10-30.
 */
public class RalColorsExtractor implements Extractor {

    public int extract(final String inputFileName, final String outputDirectoryName) throws IOException {
        final Document doc = Jsoup.parse(new File(inputFileName), "UTF-8");
        final Element table = doc.getElementsByTag("table").first();


        final File out = new File(outputDirectoryName.concat(ExtractUtils.EXTRACTED).concat("ral.csv"));
        final FileWriter fw = new FileWriter(out);
        fw.write("r,g,b,color\r\n");


        final Elements rows = table.getElementsByTag("tr");
        for (final Element row : rows) {
            final Elements cells = row.getElementsByTag("td");
            if (hasColor(cells)) {
                final int index = getStartIndex(cells);
                String hex = cells.get(index + 2).text();
                String name = cells.get(index + 4).text();
                // data is buggy, fixing it
                hex = hex.replace("##", "#");
                final String data =
                        ExtractUtils.getRed(hex) + "," +
                                ExtractUtils.getGreen(hex) + "," +
                                ExtractUtils.getBlue(hex) + "," +
                                name;
                System.out.println(data);
                fw.write(data.concat("\r\n"));
            }
        }

        fw.flush();
        fw.close();
        return 0;
    }

    private int getStartIndex(final Elements cells) {
        int index = 0;
        for (Element cell : cells) {
            if (cell.text().startsWith("RAL ")) {
                return index;
            }
            index++;
        }
        throw new RuntimeException("Not found");
    }

    private boolean hasColor(final Elements cells) {
        final boolean first = (cells.size() == 11) && cells.get(1).text().startsWith("RAL ") && cells.get(3).text().startsWith("#");
        final boolean next = (cells.size() == 9) && cells.get(0).text().startsWith("RAL ") && cells.get(2).text().startsWith("#");
        return first || next;
    }


    public static void main(String[] args) throws IOException {
        String s = "C:\\workspaces\\xorlab\\openshift\\color-converter\\src\\main\\resources\\source\\RAL";
        String f = s + "\\RAL Color Chart _ www.RALcolor.com.html";
        new RalColorsExtractor().extract(f, s);
    }

}
