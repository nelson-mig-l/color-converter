package org.neocities.nelson.color.extract;

import java.io.IOException;
import java.net.URL;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Created by dacruzgo on 2015-11-18.
 */
public class WikipediaListOfColorsWalker {

    private static final String A_F = "https://en.wikipedia.org/wiki/List_of_colors:_A%E2%80%93F";
    private static final String G_M = "https://en.wikipedia.org/wiki/List_of_colors:_G%E2%80%93M";
    private static final String N_Z = "https://en.wikipedia.org/wiki/List_of_colors:_N%E2%80%93Z";

    private static final String OUT = "C:\\workspaces\\xorlab\\openshift\\color-converter\\src\\main\\resources\\unused\\wikilist\\";

    public static void main(final String[] args) throws IOException {
        final Document doc = Jsoup.parse(new URL(N_Z), 6000);
        final Element table = doc.getElementsByTag("table").get(0);
        final Elements rows = table.getElementsByTag("tr");
        for (final Element row : rows) {
            final Elements cells = row.getElementsByTag("th");
            if (cells.size() == 1) {
                print(cells);
                //System.out.println(cells.get(0).getElementsByTag("a").get(0));
            }
        }
        //System.out.print(table);
    }

    private static void print(final Elements cells) throws IOException {
        final Element cell = cells.get(0);
        final Elements links = cell.getElementsByTag("a");
        if (links.isEmpty()) {
            System.out.println(cell.text());
        } else {
            final Element link = links.get(0);
            final String url = link.attr("abs:href");
            if (url.contains("wiki")) {
                new WikipediaColorInfoBoxExtractor().extract(new URL(url), OUT);
            } else {
                System.err.println(url);
            }
        }
    }


}
