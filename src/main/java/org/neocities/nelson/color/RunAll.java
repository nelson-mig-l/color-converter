package org.neocities.nelson.color;

import org.neocities.nelson.color.convert.text.TextFormat;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dacruzgo on 2015-10-27.
 */
@Deprecated
public class RunAll {

    public static final String BASE_PATH = "D:\\repository\\openshift\\color-converter\\src\\main\\resources\\";
    public static final String INPUT_PATH = BASE_PATH.concat("source\\");
    public static final String OUTPUT_PATH = BASE_PATH.concat("output-tmp\\");

    public static final List<Parameters> ALL = new ArrayList<Parameters>();
    static {
        //ALL.add(createParameters("xkcd/rgb.txt", "xkcd.json", "XKCD", TextFormat.XKCD));

        //ALL.add(createParameters("Color-Library/Color-Library-0.021/lib/Color/Library/Dictionary/NBS_ISCC.pm", "nbs-iscc.json", "NBS/ISCC Centroids", TextFormat.PerlColorLibrary));
        //ALL.add(createParameters("Color-Library/Color-Library-0.021/lib/Color/Library/Dictionary/NBS_ISCC/A.pm", "nbs-iscc.a.json", "NBS/ISCC A", TextFormat.PerlColorLibrary));
        //ALL.add(createParameters("Color-Library/Color-Library-0.021/lib/Color/Library/Dictionary/NBS_ISCC/B.pm", "nbs-iscc.b.json", "NBS/ISCC B", TextFormat.PerlColorLibrary));
        //ALL.add(createParameters("Color-Library/Color-Library-0.021/lib/Color/Library/Dictionary/NBS_ISCC/F.pm", "nbs-iscc.F.json", "NBS/ISCC F", TextFormat.PerlColorLibrary));
        //ALL.add(createParameters("Color-Library/Color-Library-0.021/lib/Color/Library/Dictionary/NBS_ISCC/H.pm", "nbs-iscc.h.json", "NBS/ISCC H", TextFormat.PerlColorLibrary));
        //ALL.add(createParameters("Color-Library/Color-Library-0.021/lib/Color/Library/Dictionary/NBS_ISCC/M.pm", "nbs-iscc.m.json", "NBS/ISCC M", TextFormat.PerlColorLibrary));
        //ALL.add(createParameters("Color-Library/Color-Library-0.021/lib/Color/Library/Dictionary/NBS_ISCC/P.pm", "nbs-iscc.p.json", "NBS/ISCC P", TextFormat.PerlColorLibrary));
        //ALL.add(createParameters("Color-Library/Color-Library-0.021/lib/Color/Library/Dictionary/NBS_ISCC/R.pm", "nbs-iscc.r.json", "NBS/ISCC R", TextFormat.PerlColorLibrary));
        //ALL.add(createParameters("Color-Library/Color-Library-0.021/lib/Color/Library/Dictionary/NBS_ISCC/RC.pm", "nbs-iscc.rc.json", "NBS/ISCC RC", TextFormat.PerlColorLibrary));
        //ALL.add(createParameters("Color-Library/Color-Library-0.021/lib/Color/Library/Dictionary/NBS_ISCC/S.pm", "nbs-iscc.s.json", "NBS/ISCC S", TextFormat.PerlColorLibrary));
        //ALL.add(createParameters("Color-Library/Color-Library-0.021/lib/Color/Library/Dictionary/NBS_ISCC/SC.pm", "nbs-iscc.sc.json", "NBS/ISCC SC", TextFormat.PerlColorLibrary));
        //ALL.add(createParameters("Color-Library/Color-Library-0.021/lib/Color/Library/Dictionary/NBS_ISCC/TC.pm", "nbs-iscc.tc.json", "NBS/ISCC TC", TextFormat.PerlColorLibrary));

        //ALL.add(createParameters("X11/rgb.txt", "x11.json", "X11", TextFormat.X11));

        //ALL.add(createParameters("mozilla/ColorNames.txt", "mozilla.json", "Mozilla", TextFormat.Mozilla));

        ALL.add(createParameters("Crayola/_extracted/standard-colors.csv", "crayola.standard-colors.json", "Crayola Standard Colors", TextFormat.GenericCsv));
        ALL.add(createParameters("Crayola/_extracted/fluorescent-crayons.csv", "crayola.fluorescent-crayons.json", "Crayola Fluorescent Crayons", TextFormat.GenericCsv));

        //ALL.add(createParameters("Resene/Resene-2010-rgb.txt", "resene.2010.json", "Resene 2010", TextFormat.X11));

        //ALL.add(createParameters("tango/Tango-Palette.gpl", "tango.json", "Tango Icon Theme", TextFormat.GimpPalette));

        //ALL.add(createParameters("ral/_extracted/ral.csv", "ral.json", "RAL", TextFormat.GenericCsv));

        //ALL.add(createParameters("e-paint/_extracted/Australian Standard colour chart.csv", "australian-standard.json", "Australian Standard", TextFormat.GenericCsv));
        //ALL.add(createParameters("e-paint/_extracted/British Standard BS381C COLOUR CHART for paints.csv", "british-standard.381c.json", "British Standard 381C", TextFormat.GenericCsv));
        //ALL.add(createParameters("e-paint/_extracted/British Standard BS2660 COLOUR CHART.csv", "british-standard.2660.json", "British Standard 2660", TextFormat.GenericCsv));
        //ALL.add(createParameters("e-paint/_extracted/British Standard BS5252 COLOUR CHART.csv", "british-standard.5252.json", "British Standard 5252", TextFormat.GenericCsv));
        //ALL.add(createParameters("e-paint/_extracted/BS4800 COLOUR CHART_ 1989.csv", "british-standard.4800.1989.json", "British Standard 4800:1989", TextFormat.GenericCsv));
        //ALL.add(createParameters("e-paint/_extracted/British Standard BS 4800 COLOUR CHART for paints.csv", "british-standard.4800.2011.json", "British Standard 2660", TextFormat.GenericCsv));

        //ALL.add(createParameters("e-paint/_extracted/Other British Standard Colour Chart.csv", "british-standard.other.json", "British Standard 2660", TextFormat.GenericCsv));
        //ALL.add(createParameters("e-paint/_extracted/Federal standard 595 color chart.csv", "federal-standard.595.json", "British Standard 2660", TextFormat.GenericCsv));

        //ALL.add(createParameters("colorhexa/_extracted/[not-found]-b86fdd01.csv", "colorhexa.json", "ColorHexa", TextFormat.GenericCsv));

        //ALL.add(createParameters("benjaminmoore/_extracted/affinity.csv", "benjamin-moore.affinity.json", "Benjamin Moore Affinity", TextFormat.GenericCsv));
        //ALL.add(createParameters("benjaminmoore/_extracted/classic-colors.csv", "benjamin-moore.classic-colors.json", "Benjamin Moore Classic Colors", TextFormat.GenericCsv));
        //ALL.add(createParameters("benjaminmoore/_extracted/color-preview.csv", "benjamin-moore.color-preview.json", "Benjamin Moore Color Preview", TextFormat.GenericCsv));
        //ALL.add(createParameters("benjaminmoore/_extracted/color-stories.csv", "benjamin-moore.color-stories.json", "Benjamin Moore Color Stories", TextFormat.GenericCsv));
        //ALL.add(createParameters("benjaminmoore/_extracted/designer-colors.csv", "benjamin-moore.designer-colors.json", "Benjamin Moore Designer Colors", TextFormat.GenericCsv));
        //ALL.add(createParameters("benjaminmoore/_extracted/historical-colors.csv", "benjamin-moore.historical-colors.json", "Benjamin Moore Historical Colors", TextFormat.GenericCsv));
        //ALL.add(createParameters("benjaminmoore/_extracted/off-white-colors.csv", "benjamin-moore.off-white-colors.json", "Benjamin Moore Off White Colors", TextFormat.GenericCsv));


        //ALL.add(createParameters("solarized/solarized.txt", "solarized.json", "Solarized", TextFormat.GimpPalette));

    }

    private static Parameters createParameters(final String in, final String out, final String palette, final TextFormat format) {
        final File input = new File(INPUT_PATH.concat(in));
        final File output = new File(OUTPUT_PATH.concat(out));
        return Parameters.create(input, output, palette, format);
    }

    public static void main(final String[] args) throws Exception {
        int acc = 0;
        final Convert convert = new Convert();
        for (final Parameters parameters : ALL) {
            try {
                acc += convert.run(parameters);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Thread.sleep(100);
        }
        System.out.println(acc + " colors added");

        new Counter().run(OUTPUT_PATH);

        System.out.println("Bye!");
    }
}
