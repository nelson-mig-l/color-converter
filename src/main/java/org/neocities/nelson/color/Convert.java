package org.neocities.nelson.color;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.neocities.nelson.color.convert.Converter;
import org.neocities.nelson.color.convert.ConverterFactory;
import org.neocities.nelson.color.model.ColorContainer;

import java.io.IOException;

/**
 * Created by dacruzgo on 2015-10-22.
 */
public class Convert {

    //public static final String FILE_PATH = "C:\\workspaces\\xorlab\\openshift\\color-converter\\src\\main\\resources\\xkcd\\rgb.txt";
    public static final String FILE_PATH = "C:\\workspaces\\xorlab\\openshift\\color-converter\\src\\main\\resources\\nbs-iscc\\NBS_ISCC.pm";

//    public static void main(final String[] args) throws IOException {
//        //final Palette palette = Palette.XKCD;
//        final File input = new File(FILE_PATH);
//        final File output = new File(FILE_PATH.concat(".json"));
//
//        final Parameters parameters = new Parameters();
//        parameters.setInput(input);
//        parameters.setOutput(output);
//        parameters.setPalette("XKCD");
//        parameters.setFormat(TextFormat.XKCD);
//
//        new Convert().run(parameters);
//    }

    public int run(final Parameters parameters) throws IOException {
        final String palette = parameters.getPalette();

        System.out.println("Reading '" + parameters.getInput() + "' for '" + palette + "' palette.");

        final Converter converter = ConverterFactory.converter(parameters.getFormat());
        final ColorContainer colors = converter.convert(parameters.getInput());

        System.out.println(colors.size() + " colors parsed");

        final ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationConfig.Feature.INDENT_OUTPUT);

        System.out.println("Writing to '" + parameters.getOutput() + "'.");

        mapper.writeValue(parameters.getOutput(), colors.colors(palette));

        System.out.println("Done");

        return colors.size();
    }
}
