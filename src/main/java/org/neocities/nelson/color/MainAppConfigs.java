package org.neocities.nelson.color;

import org.neocities.nelson.color.convert.text.TextFormat;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nelson on 10/25/2016.
 */
public class MainAppConfigs {

    private static final String BASE_PATH = "D:\\repository\\openshift\\color-data\\";
    public static final String SOURCE_PATH = BASE_PATH.concat("source\\");
    public static final String TARGET_PATH = BASE_PATH.concat("target\\");

    public static final List<Parameters> ALL = new ArrayList<Parameters>();
    static {
        ALL.add(createParameters("xkcd/rgb.txt", "xkcd.json", "xkcd", TextFormat.XKCD));
        ALL.add(createParameters("x11/rgb.txt", "x11.json", "X11", TextFormat.X11));

        ALL.add(createParameters("solarized/solarized.txt", "solarized.json", "Solarized", TextFormat.GimpPalette));
        ALL.add(createParameters("tango/Tango-Palette.gpl", "tango.json", "Tango Icon Theme", TextFormat.GimpPalette));

        ALL.add(createParameters("mozilla/ColorNames.txt", "mozilla.json", "Mozilla", TextFormat.Mozilla));

        ALL.add(createParameters("perl-color-library/NBS_ISCC.pm", "nbs-iscc.json", "NBS/ISCC Centroids", TextFormat.PerlColorLibrary));
        ALL.add(createParameters("perl-color-library/NBS_ISCC/A.pm", "nbs-iscc.a.json", "NBS/ISCC A", TextFormat.PerlColorLibrary));
        ALL.add(createParameters("perl-color-library/NBS_ISCC/B.pm", "nbs-iscc.b.json", "NBS/ISCC B", TextFormat.PerlColorLibrary));
        ALL.add(createParameters("perl-color-library/NBS_ISCC/F.pm", "nbs-iscc.F.json", "NBS/ISCC F", TextFormat.PerlColorLibrary));
        ALL.add(createParameters("perl-color-library/NBS_ISCC/H.pm", "nbs-iscc.h.json", "NBS/ISCC H", TextFormat.PerlColorLibrary));
        ALL.add(createParameters("perl-color-library/NBS_ISCC/M.pm", "nbs-iscc.m.json", "NBS/ISCC M", TextFormat.PerlColorLibrary));
        ALL.add(createParameters("perl-color-library/NBS_ISCC/P.pm", "nbs-iscc.p.json", "NBS/ISCC P", TextFormat.PerlColorLibrary));
        ALL.add(createParameters("perl-color-library/NBS_ISCC/R.pm", "nbs-iscc.r.json", "NBS/ISCC R", TextFormat.PerlColorLibrary));
        ALL.add(createParameters("perl-color-library/NBS_ISCC/RC.pm", "nbs-iscc.rc.json", "NBS/ISCC RC", TextFormat.PerlColorLibrary));
        ALL.add(createParameters("perl-color-library/NBS_ISCC/S.pm", "nbs-iscc.s.json", "NBS/ISCC S", TextFormat.PerlColorLibrary));
        ALL.add(createParameters("perl-color-library/NBS_ISCC/SC.pm", "nbs-iscc.sc.json", "NBS/ISCC SC", TextFormat.PerlColorLibrary));
        ALL.add(createParameters("perl-color-library/NBS_ISCC/TC.pm", "nbs-iscc.tc.json", "NBS/ISCC TC", TextFormat.PerlColorLibrary));

        ALL.add(createParameters("benjamin-moore/BenjaminMoore_Affinity_en-us.acb", "benjamin-moore-affinity.json", "Benjamin Moore Affinity", TextFormat.AutoCADPalette));
        ALL.add(createParameters("benjamin-moore/BenjaminMoore_AmericasColors_en-us.acb", "benjamin-moore-americas-colors.json", "Benjamin Moore Americas Colors", TextFormat.AutoCADPalette));
        ALL.add(createParameters("benjamin-moore/BenjaminMoore_ClassicColors_en-us.acb", "benjamin-moore-classic-colors.json", "Benjamin Moore Classic Colors", TextFormat.AutoCADPalette));
        ALL.add(createParameters("benjamin-moore/BenjaminMoore_ColorPreview_en-us.acb", "benjamin-moore-color-preview.json", "Benjamin Moore Color Preview", TextFormat.AutoCADPalette));
        ALL.add(createParameters("benjamin-moore/BenjaminMoore_ColorStories_en-us.acb", "benjamin-moore-color-stories.json", "Benjamin Moore Color Stories", TextFormat.AutoCADPalette));
        ALL.add(createParameters("benjamin-moore/BenjaminMoore_ColorTrends2016_en-us.acb", "benjamin-moore-color-trends-2016.json", "Benjamin Moore Color Trends 2016", TextFormat.AutoCADPalette));
        ALL.add(createParameters("benjamin-moore/BenjaminMoore_DesignerClassics_en-us.acb", "benjamin-moore-designer-classics.json", "Benjamin Moore Designer Classics", TextFormat.AutoCADPalette));

        ALL.add(createParameters("benjamin-moore/BenjaminMoore_HistoricalColors_en-us.acb", "benjamin-moore-historical-colors.json", "Benjamin Moore Historical Colors", TextFormat.AutoCADPalette));
        ALL.add(createParameters("benjamin-moore/BenjaminMoore_Off-WhiteColors_en-us.acb", "benjamin-moore-off-white-colors.json", "Benjamin Moore Off White Colors", TextFormat.AutoCADPalette));
        ALL.add(createParameters("benjamin-moore/BenjaminMoore_REVIVEColorsForVinylSiding_en-us.acb", "benjamin-moore-revive.json", "Benjamin Moore Revive", TextFormat.AutoCADPalette));
        ALL.add(createParameters("benjamin-moore/BenjaminMoore_WILLIAMSBURG_ColorCollection_en-us.acb", "benjamin-moore-williams-burg.json", "Benjamin Moore Williams Burg", TextFormat.AutoCADPalette));

        ALL.add(createParameters("e-paint/Australian Standard colour chart.html", "australian-standard.json", "Australian Standard", TextFormat.EPaintHtmlPalette));

        ALL.add(createParameters("e-paint/British Standard BS 4800 COLOUR CHART for paints.html", "british-standard-4800.json", "British Standard BS4800", TextFormat.EPaintHtmlPalette));
        ALL.add(createParameters("e-paint/British Standard BS381C COLOUR CHART for paints.html", "british-standard-381c.json", "British Standard BS381C", TextFormat.EPaintHtmlPalette));
        ALL.add(createParameters("e-paint/British Standard BS2660 COLOUR CHART.html", "british-standard-2660.json", "British Standard BS2660", TextFormat.EPaintHtmlPalette));
        ALL.add(createParameters("e-paint/British Standard BS5252 COLOUR CHART.html", "british-standard-5252.json", "Australian Standard BS5252", TextFormat.EPaintHtmlPalette));

        ALL.add(createParameters("e-paint/Federal standard 595C color chart(FLAT.MATT).html", "federal-standard-595c-matt.json", "Federal Standard 595C Matt", TextFormat.EPaintHtmlPalette));
        ALL.add(createParameters("e-paint/Federal standard 595C color chart(GLOSS).html", "federal-standard-595c-gloss.json", "Federal Standard 595C Gloss", TextFormat.EPaintHtmlPalette));
        ALL.add(createParameters("e-paint/Federal standard 595C color chart(SEMI-GLOSS).html", "federal-standard-595c-semi-gloss.json", "Federal Standard 595C Semi Gloss", TextFormat.EPaintHtmlPalette));

        ALL.add(createParameters("e-paint/RAL Colour Chart - color charts, fans, cards, swatches and books..html", "ral-classic.json", "RAL Classic", TextFormat.EPaintHtmlPalette));

        ALL.add(createParameters("e-paint/Other British Standard Colour Chart.html", "british-standard-other.json", "British Standard Other", TextFormat.EPaintHtmlPalette));

        ALL.add(createParameters("e-paint/Dulux Trade color chart.html", "dulux-trade.json", "Dulux Trade", TextFormat.EPaintHtmlPalette));

        ALL.add(createParameters("resene/Order Resene Paint Drawdowns - Decks_driveways range.html", "resene-decks-driveways-range.json", "Resene Decks Driveways Range", TextFormat.ReseneHtmlFormat));
        ALL.add(createParameters("resene/Order Resene Paint Drawdowns - Heritage range.html", "resene-heritage-range.json", "Resene Heritage Range", TextFormat.ReseneHtmlFormat));
        ALL.add(createParameters("resene/Order Resene Paint Drawdowns - Karen Walker.html", "resene-karen-walker.json", "Resene Karen Walker", TextFormat.ReseneHtmlFormat));
        ALL.add(createParameters("resene/Order Resene Paint Drawdowns - KidzColour.html", "resene-kidz-colour.json", "Resene Kidz Colour", TextFormat.ReseneHtmlFormat));
        ALL.add(createParameters("resene/Order Resene Paint Drawdowns - Metallics and special effects.html", "resene-metallics-and-sepcial-effects.json", "Resene Metallics And Special Effects", TextFormat.ReseneHtmlFormat));
        ALL.add(createParameters("resene/Order Resene Paint Drawdowns - Multi-finish (2016, R series).html", "resene-multi-finish-2016-r.json", "Resene Multi-finish 2016 R series", TextFormat.ReseneHtmlFormat));
        ALL.add(createParameters("resene/Order Resene Paint Drawdowns - Multi-finish (P series palettes).html", "resene-multi-finish-p.json", "Resene Multi-finish P series palettes", TextFormat.ReseneHtmlFormat));
        ALL.add(createParameters("resene/Order Resene Paint Drawdowns - Roof Systems (Summit).html", "resene-roof-systems-summit.json", "Resene Roof Systems Summit", TextFormat.ReseneHtmlFormat));
        ALL.add(createParameters("resene/Order Resene Paint Drawdowns - The Range fashion colours 18.html", "resene-fashion-colors-18.json", "Resene The Range Fashion Colors 18", TextFormat.ReseneHtmlFormat));
        ALL.add(createParameters("resene/Order Resene Paint Drawdowns - The Range whites & neutrals (solid colours).html", "resene-white-and-neutrals-solid.json", "Resene The Range Whites and Neutrals Solid", TextFormat.ReseneHtmlFormat));
        ALL.add(createParameters("resene/Order Resene Paint Drawdowns - The Range whites & neutrals (wood stains).html", "resene-white-and-neutrals-wood.json", "Resene The Range Whites and Neutrals Wood", TextFormat.ReseneHtmlFormat));
        ALL.add(createParameters("resene/Order Resene Paint Drawdowns - Woodsman.html", "resene-woodsman.json", "Resene Woodsman", TextFormat.ReseneHtmlFormat));

        ALL.add(createParameters("colorhexa/List of colors.html", "colorhexa.json", "Color Hexa", TextFormat.GenericHtmlTableFormat));
    }

    private static Parameters createParameters(final String in, final String out, final String palette, final TextFormat format) {
        final File input = new File(SOURCE_PATH.concat(in));
        final File output = new File(TARGET_PATH.concat(out));
        return Parameters.create(input, output, palette, format);
    }
}
