package org.neocities.nelson.color;

import java.io.IOException;

/**
 * Created by Nelson on 10/25/2016.
 */
public class MainApp {

    public static void main(final String[] args) throws IOException {
        final Convert convert = new Convert();

        int total = 0;
        for (final Parameters parameters : MainAppConfigs.ALL) {
            final int converted = convert.run(parameters);
            total += converted;
        }


        new Counter().run(MainAppConfigs.TARGET_PATH);
    }

}
