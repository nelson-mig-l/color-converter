package org.neocities.nelson.color;

import org.neocities.nelson.color.convert.text.TextFormat;

import java.io.File;

/**
 * Created by dacruzgo on 2015-10-27.
 */
public class Parameters {

    private File input;
    private File output;
    private String palette;
    private TextFormat format;

    public static Parameters create(final File input, final File output, final String palette, final TextFormat format) {
        final Parameters instance = new Parameters();
        instance.setInput(input);
        instance.setOutput(output);
        instance.setPalette(palette);
        instance.setFormat(format);
        return instance;
    }

    public File getInput() {
        return input;
    }

    public void setInput(File input) {
        this.input = input;
    }

    public File getOutput() {
        return output;
    }

    public void setOutput(File output) {
        this.output = output;
    }

    public String getPalette() {
        return palette;
    }

    public void setPalette(String palette) {
        this.palette = palette;
    }

    public TextFormat getFormat() {
        return format;
    }

    public void setFormat(TextFormat format) {
        this.format = format;
    }
}
