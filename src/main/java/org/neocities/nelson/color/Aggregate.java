package org.neocities.nelson.color;

import org.apache.commons.io.LineIterator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.util.HashSet;

/**
 * Created by dacruzgo on 2015-11-19.
 */
@Deprecated
public class Aggregate {

    private static final String DIR = "C:\\workspaces\\xorlab\\openshift\\color-converter\\src\\main\\resources\\unused\\wikilist\\_extracted";
    private static final FilenameFilter CSV_FILE_FILTER = new FilenameFilter() {
        public boolean accept(final File dir, final String name) {
            return name.endsWith(".csv");
        }
    };

    private final HashSet<String> result = new HashSet<String>();

    public static void main(final String[] args) throws FileNotFoundException {
        final File dir = new File(DIR);
        final File[] files = dir.listFiles(CSV_FILE_FILTER);

        final Aggregate aggregate = new Aggregate();
        for (final File file : files) {
            //System.out.println(file);
            aggregate.collect(file);
        }

        final HashSet<String> all = aggregate.get();
        System.out.println(all.size());
        for (final String s : all) {
            System.out.println(">>> " + s);
        }
    }

    public void collect(final File file) throws FileNotFoundException {
        final FileReader reader = new FileReader(file);
        LineIterator lit = new LineIterator(reader);
        while (lit.hasNext()) {
            final String line = lit.next();
            final String[] tokens = line.split(",");
            final String last = tokens[tokens.length - 1].replaceAll("\\[.*?\\]", "");
            //System.out.println(last);
            result.add(last);
        }
    }

    public HashSet<String> get() {
        return result;
    }

}
